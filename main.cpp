#include <iostream>
#include "Logger.h"
_INITIALIZE_EASYLOGGINGPP

int main()
{
    Logger::configLogger();
    Logger::log(Logger::INFO, "My first info log");
    Logger::log(Logger::FATAL, "Just another test");
    Logger::log(Logger::DEBUG, "This is a debug msg");
    return 0;
}
