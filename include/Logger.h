#ifndef LOGGER_H
#define LOGGER_H

#include "string"
#include "easylogging++.h"


class Logger
{
    public:
        Logger();
        virtual ~Logger();
        enum LOGGER_LEVEL
        {
            INFO,
            DEBUG,
            WARNING,
            FATAL,
        };
        static void log(LOGGER_LEVEL lvl, std::string msg);
        static void configLogger();
    protected:
    private:
};

#endif // LOGGER_H
