#include "Logger.h"

#define pathToLogFile "./logs/"

Logger::Logger()
{
    //ctor
}

Logger::~Logger()
{
    //dtor
}

void Logger::log(LOGGER_LEVEL lvl, std::string msg)
{
    switch(lvl)
    {
        case INFO:
            LOG(INFO) << msg;
            break;
        case DEBUG:
            LOG(DEBUG) << msg;
            break;
        case FATAL:
            LOG(FATAL) << msg;
        default:
            break;
    }
}

void Logger::configLogger()
{
    easyloggingpp::Loggers::setFilename(pathToLogFile);
}
